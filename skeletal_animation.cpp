#include <ktnEngine/ktnApplication>
#include <ktnEngine/ktnGraphics>

#include <glm/gtc/matrix_transform.hpp>

#include <random>

using namespace glm;
using namespace ktn;
using namespace ktn::Application;
using namespace ktn::Graphics;
using namespace ktn::Physics;
using namespace std;

class saScene : public ktnScene {
public:
    class saSceneInputProcessor : public IktnInputProcessor {
    public:
        saSceneInputProcessor(GLFWwindow *eWindow) {
            m_Window = eWindow;
            m_InputMapper.Initialize();
        }
        void ProcessInput() {
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_ESCAPE]) == GLFW_PRESS) { glfwSetWindowShouldClose(m_Window, 1); }

            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_W]) == GLFW_PRESS) { KeyPressed.Emit(KTNKEY_W); }
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_A]) == GLFW_PRESS) { KeyPressed.Emit(KTNKEY_A); }
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_S]) == GLFW_PRESS) { KeyPressed.Emit(KTNKEY_S); }
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_D]) == GLFW_PRESS) { KeyPressed.Emit(KTNKEY_D); }
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_UP]) == GLFW_PRESS) { KeyPressed.Emit(KTNKEY_UP); }
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_DOWN]) == GLFW_PRESS) { KeyPressed.Emit(KTNKEY_DOWN); }
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_LEFT]) == GLFW_PRESS) { KeyPressed.Emit(KTNKEY_LEFT); }
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_RIGHT]) == GLFW_PRESS) { KeyPressed.Emit(KTNKEY_RIGHT); }
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_SPACE]) == GLFW_PRESS) { KeyPressed.Emit(KTNKEY_SPACE); }
        }

        ktnSignal<ktnKey> KeyPressed;

    private:
        GLFWwindow *m_Window;

        ktnInputMapper m_InputMapper;
    };

    saScene( //
        ktnGraphicsScenePtr eGraphicsScene,
        ktnPhysicsScenePtr ePhysicsScene,
        shared_ptr<saSceneInputProcessor> eInputProcessor)
        : ktnScene(eGraphicsScene, ePhysicsScene) {
        auto camera = make_shared<ktnCamera>();
        camera->SetPosition({-2.f, 1.0f, 2.0f}, WorldSpace);
        camera->TurnLeft(450);
        camera->TurnDown(150);
        m_GraphicsScene->SetCurrentCamera(camera);

        srand(static_cast<uint32_t>(std::chrono::steady_clock::now().time_since_epoch().count()));

        auto light = make_shared<ktnLight>(string("light").c_str());
        light->SetPosition(m_GraphicsScene->CurrentCamera()->Position(WorldSpace) + m_GraphicsScene->CurrentCamera()->Front() * 0.1f, WorldSpace);
        light->SetDirection(m_GraphicsScene->CurrentCamera()->Front());

        light->Intensity = 10;
        m_GraphicsScene->AddLight(light);

        for (int i = 0; i < 10; ++i) {
            auto lightIter = make_shared<ktnLight>((string("light") + to_string(i)).c_str());
            lightIter->Intensity = 0.5f * (rand() % 10);
            lightIter->SetPosition(vec3(0.1f * (rand() % 100 - 50), 0.1f * (rand() % 100 - 50), 0.1f * (rand() % 100 - 50)), WorldSpace);
            lightIter->SetColor(0.01f * (rand() % 100), 0.01f * (rand() % 100), 0.01f * (rand() % 100));
            lightIter->Falloff.LinearTerm = 0.5f;
            lightIter->Falloff.QuadraticTerm = 0.5f;
            m_GraphicsScene->AddLight(lightIter);
        }

        InputProcessor = eInputProcessor;
        dynamic_cast<saSceneInputProcessor *>(InputProcessor.get())->KeyPressed.Connect(this, &saScene::HandleEvent_KeyPressed);
    }

    void HandleEvent_KeyPressed(ktnKey eKey) {
        float cameraSpeed = 2.0f * m_DeltaTime;
        switch (eKey) {
        case KTNKEY_W:
            m_GraphicsScene->CurrentCamera()->MoveForward(cameraSpeed);
            break;
        case KTNKEY_A:
            m_GraphicsScene->CurrentCamera()->MoveLeft(cameraSpeed);
            break;
        case KTNKEY_S:
            m_GraphicsScene->CurrentCamera()->MoveBackward(cameraSpeed);
            break;
        case KTNKEY_D:
            m_GraphicsScene->CurrentCamera()->MoveRight(cameraSpeed);
            break;
        case KTNKEY_UP:
            m_GraphicsScene->CurrentCamera()->TurnUp(1);
            m_GraphicsScene->CurrentCamera()->UpdateFront();
            m_GraphicsScene->CurrentCamera()->UpdateViewMatrix();
            break;
        case KTNKEY_DOWN:
            m_GraphicsScene->CurrentCamera()->TurnDown(1);
            m_GraphicsScene->CurrentCamera()->UpdateFront();
            m_GraphicsScene->CurrentCamera()->UpdateViewMatrix();
            break;

        case KTNKEY_LEFT:
            m_GraphicsScene->CurrentCamera()->TurnLeft(1);
            m_GraphicsScene->CurrentCamera()->UpdateFront();
            m_GraphicsScene->CurrentCamera()->UpdateViewMatrix();
            break;
        case KTNKEY_RIGHT:
            m_GraphicsScene->CurrentCamera()->TurnRight(1);
            m_GraphicsScene->CurrentCamera()->UpdateFront();
            m_GraphicsScene->CurrentCamera()->UpdateViewMatrix();
            break;

        case KTNKEY_SPACE:
            PlayAnimation.Emit("Flick");
            break;

        default:
            break;
        }
    }

    void ProcessInput() final {
        auto currentFrameTime = static_cast<float>(glfwGetTime());
        m_DeltaTime = currentFrameTime - m_LastFrameTime;
        m_LastFrameTime = currentFrameTime;

        InputProcessor->ProcessInput();

        m_GraphicsScene->CurrentCamera()->UpdateViewMatrix();

        if (m_LeftMouseIsClicking) {
            m_GraphicsScene->Lights[0]->SetPosition(
                m_GraphicsScene->CurrentCamera()->Position(WorldSpace) + m_GraphicsScene->CurrentCamera()->Front() * 0.1f, WorldSpace);
            m_GraphicsScene->Lights[0]->SetDirection(m_GraphicsScene->CurrentCamera()->Front());
        }
    }

    void HandleEvent_MouseClicked(int button, int action, int mods) final {
        // for the time being the modification does nothing, but can be added to do stuff later.
        if (mods != 0) {}

        if (GLFW_MOUSE_BUTTON_LEFT == button) {
            if (action == GLFW_PRESS) { m_LeftMouseIsClicking = true; }
            if (action == GLFW_RELEASE) { m_LeftMouseIsClicking = false; }
        }
    }

    void HandleEvent_MouseMoved(double xpos, double ypos) final {
        if (m_FirstMouse) {
            m_LastX = xpos;
            m_LastY = ypos;
            m_FirstMouse = false;
        }
        m_GraphicsScene->CurrentCamera()->TurnRight(float(xpos - m_LastX));
        m_GraphicsScene->CurrentCamera()->TurnDown(float(ypos - m_LastY));
        m_GraphicsScene->CurrentCamera()->UpdateFront();
        m_GraphicsScene->CurrentCamera()->UpdateViewMatrix();
        m_LastX = xpos;
        m_LastY = ypos;
    }

    ktnSignal<string> PlayAnimation;
    shared_ptr<IktnInputProcessor> InputProcessor;
};

int main() {
    ktnApplicationWindow window(vk::Extent2D(640, 480));

    auto app = ktnApplication::CreateApplicationWithDefaultRenderer<ktnRenderer_OpenGL_ImGui>();
    window.WindowName = "Skeletal Animation example";
    window.Initialize(app);
    window.HideCursor();

    auto scene = make_shared<saScene>( //
        make_shared<ktnGraphicsScene>(),
        make_shared<ktnPhysicsScene>(),
        make_shared<saScene::saSceneInputProcessor>(window.Window));

    // This is particularly leaking memory.
    // If we create normal variables here they will go out of scope after the if
    // We keep it for the time being, as the vulkan backend needs to have the
    // shader and model classes changed as well.
    if (app->Renderer->Backend() == ktnGraphicsBackend::OPENGL) {
        auto robot = make_shared<ktnModel>("resources/models/skeletal_animation.gltf");

        scene->PlayAnimation.Connect(robot.get(), &ktnModel::StartAction);

        for (auto &action : robot->Actions) {
            cout << "Action \"" << action.second->Name << "\", Duration: " << action.second->Duration() << " second(s)" << endl;
        }
        robot->Name = "skelly_model";
        robot->SetPosition({0, 0.2F, 0.5}, WorldSpace);
        scene->GraphicsScene()->AddModel(robot);
    }

    app->SetActiveScene(scene);

    // render loop
    window.Run();
    return 0;
}
