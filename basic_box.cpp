﻿#include <ktnEngine/ktnApplication>
#include <ktnEngine/ktnGraphics>

#include <glm/gtc/matrix_transform.hpp>

#include <random>

using namespace glm;
using namespace ktn;
using namespace ktn::Application;
using namespace ktn::Core;
using namespace ktn::Graphics;
using namespace ktn::Physics;
using namespace std;

class bbScene : public ktnScene {
public:
    class bbSceneInputProcessor : public IktnInputProcessor {
    public:
        bbSceneInputProcessor(GLFWwindow *eWindow) {
            m_Window = eWindow;
            m_InputMapper.Initialize();
        }
        void ProcessInput() {
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_ESCAPE]) == GLFW_PRESS) { glfwSetWindowShouldClose(m_Window, 1); }

            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_W]) == GLFW_PRESS) { KeyPressed.Emit(KTNKEY_W); }
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_A]) == GLFW_PRESS) { KeyPressed.Emit(KTNKEY_A); }
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_S]) == GLFW_PRESS) { KeyPressed.Emit(KTNKEY_S); }
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_D]) == GLFW_PRESS) { KeyPressed.Emit(KTNKEY_D); }
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_UP]) == GLFW_PRESS) { KeyPressed.Emit(KTNKEY_UP); }
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_DOWN]) == GLFW_PRESS) { KeyPressed.Emit(KTNKEY_DOWN); }
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_LEFT]) == GLFW_PRESS) { KeyPressed.Emit(KTNKEY_LEFT); }
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_RIGHT]) == GLFW_PRESS) { KeyPressed.Emit(KTNKEY_RIGHT); }
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_SPACE]) == GLFW_PRESS) { KeyPressed.Emit(KTNKEY_SPACE); }
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_LEFT_CONTROL]) == GLFW_PRESS) { KeyPressed.Emit(KTNKEY_LEFT_CONTROL); }
        }

        ktnSignal<ktnKey> KeyPressed;

    private:
        GLFWwindow *m_Window;

        ktnInputMapper m_InputMapper;
    };

    bbScene( //
        ktnGraphicsScenePtr eGraphicsScene,
        ktnPhysicsScenePtr ePhysicsScene,
        shared_ptr<bbSceneInputProcessor> eInputProcessor)
        : ktnScene(eGraphicsScene, ePhysicsScene) {

        auto camera = make_shared<ktnCamera>();
        camera->SetPosition(vec3{-2, 2, 4}, ReferenceSpace::World);

        camera->TurnLeft(450);
        camera->TurnDown(150);
        camera->UpdateFront();
        camera->UpdateViewMatrix();

        m_GraphicsScene->AddCamera(camera);
        m_GraphicsScene->SetCurrentCamera(camera);

        srand(static_cast<unsigned int>(std::chrono::steady_clock::now().time_since_epoch().count()));

        for (int i = 0; i < 50; ++i) {
            auto light = make_shared<ktnLight>((string("light") + to_string(i)).c_str());
            light->Intensity = 0.1f * (rand() % 10);
            light->SetPosition(vec3(0.1f * (rand() % 100 - 50), 0.1f * (rand() % 100 - 50), 0.1f * (rand() % 100 - 50)), WorldSpace);
            light->SetColor(0.01f * (rand() % 100), 0.01f * (rand() % 100), 0.01f * (rand() % 100));
            light->Falloff.LinearTerm = 0.5f;
            light->Falloff.QuadraticTerm = 0.5f;
            m_GraphicsScene->AddLight(light);
        }

        m_GraphicsScene->Lights[0]->SetPosition(
            m_GraphicsScene->CurrentCamera()->Position(WorldSpace) + m_GraphicsScene->CurrentCamera()->Front() * 0.1f, WorldSpace);
        m_GraphicsScene->Lights[0]->SetDirection(m_GraphicsScene->CurrentCamera()->Front());
        m_GraphicsScene->Lights[0]->Intensity = 100;

        InputProcessor = eInputProcessor;
        dynamic_cast<bbSceneInputProcessor *>(InputProcessor.get())->KeyPressed.Connect(this, &bbScene::HandleEvent_KeyPressed);
    }

    void ProcessInput() final {
        auto currentFrameTime = static_cast<float>(glfwGetTime());
        m_DeltaTime = currentFrameTime - m_LastFrameTime;
        m_LastFrameTime = currentFrameTime;

        InputProcessor->ProcessInput();

        m_GraphicsScene->CurrentCamera()->UpdateViewMatrix();

        if (m_LeftMouseIsClicking) {
            m_GraphicsScene->Lights[0]->SetPosition(
                m_GraphicsScene->CurrentCamera()->Position(WorldSpace) + m_GraphicsScene->CurrentCamera()->Front() * 0.1f, WorldSpace);
            m_GraphicsScene->Lights[0]->SetDirection(m_GraphicsScene->CurrentCamera()->Front());
        }

        for (size_t i = 1; i < m_GraphicsScene->Lights.size(); ++i) {
            auto light = m_GraphicsScene->Lights[i].get();
            light->Intensity = std::clamp(light->Intensity + 0.01f * (rand() % 10 - 5), 0.5f, 5.0f);
            light->OffsetPosition(vec3(0.001f * (rand() % 100 - 50), 0.001f * (rand() % 100 - 50), 0.001f * (rand() % 100 - 50)), WorldSpace);
            if (abs(light->Position(WorldSpace).x) > 4) { //
                light->SetPosition(light->Position(WorldSpace) / 4.0F, WorldSpace);
            }
            if (abs(light->Position(WorldSpace).z) > 4) { light->SetPosition(light->Position(WorldSpace) / 4.0F, WorldSpace); }

            if (light->Position(WorldSpace).y > 2) { light->SetPosition(light->Position(WorldSpace) / 2.0F, WorldSpace); }
            if (light->Position(WorldSpace).y < 0) { light->OffsetPosition({0, 0.5, 0}, WorldSpace); }
        }
    }

    void HandleEvent_KeyPressed(ktnKey eKey) {
        float cameraSpeed = 2.0f * m_DeltaTime;
        switch (eKey) {
        case KTNKEY_W:
            m_GraphicsScene->CurrentCamera()->MoveForward(cameraSpeed);
            break;
        case KTNKEY_A:
            m_GraphicsScene->CurrentCamera()->MoveLeft(cameraSpeed);
            break;
        case KTNKEY_S:
            m_GraphicsScene->CurrentCamera()->MoveBackward(cameraSpeed);
            break;
        case KTNKEY_D:
            m_GraphicsScene->CurrentCamera()->MoveRight(cameraSpeed);
            break;
        case KTNKEY_UP:
            m_GraphicsScene->CurrentCamera()->TurnUp(1);
            m_GraphicsScene->CurrentCamera()->UpdateFront();
            m_GraphicsScene->CurrentCamera()->UpdateViewMatrix();
            break;
        case KTNKEY_DOWN:
            m_GraphicsScene->CurrentCamera()->TurnDown(1);
            m_GraphicsScene->CurrentCamera()->UpdateFront();
            m_GraphicsScene->CurrentCamera()->UpdateViewMatrix();
            break;

        case KTNKEY_LEFT:
            m_GraphicsScene->CurrentCamera()->TurnLeft(1);
            m_GraphicsScene->CurrentCamera()->UpdateFront();
            m_GraphicsScene->CurrentCamera()->UpdateViewMatrix();
            break;
        case KTNKEY_RIGHT:
            m_GraphicsScene->CurrentCamera()->TurnRight(1);
            m_GraphicsScene->CurrentCamera()->UpdateFront();
            m_GraphicsScene->CurrentCamera()->UpdateViewMatrix();
            break;
        case KTNKEY_SPACE:
            m_GraphicsScene->CurrentCamera()->OffsetPosition({0, 1.F * cameraSpeed, 0}, WorldSpace);
            break;
        case KTNKEY_LEFT_CONTROL:
            m_GraphicsScene->CurrentCamera()->OffsetPosition({0, -1.F * cameraSpeed, 0}, WorldSpace);
            break;
        default:
            break;
        }
    }

    void HandleEvent_MouseClicked(int button, int action, int mods) final {
        // for the time being the modification does nothing, but can be added to do stuff later.
        if (mods != 0) {}

        if (GLFW_MOUSE_BUTTON_LEFT == button) {
            if (action == GLFW_PRESS) { m_LeftMouseIsClicking = true; }
            if (action == GLFW_RELEASE) { m_LeftMouseIsClicking = false; }
        }
    }

    void HandleEvent_MouseMoved(double xpos, double ypos) final {
        if (m_FirstMouse) {
            m_LastX = xpos;
            m_LastY = ypos;
            m_FirstMouse = false;
        }
        m_GraphicsScene->CurrentCamera()->TurnRight(float(xpos - m_LastX));
        m_GraphicsScene->CurrentCamera()->TurnDown(float(ypos - m_LastY));
        m_GraphicsScene->CurrentCamera()->UpdateFront();
        m_GraphicsScene->CurrentCamera()->UpdateViewMatrix();
        m_LastX = xpos;
        m_LastY = ypos;
    }

    ktnSignal<const vec3 &, ReferenceSpace> OffsetCamera;
};

int main() {
    auto renderer = make_shared<ktn::Graphics::ktnRenderer_OpenGL_ImGui>();
    auto game = make_shared<ktnApplication>(renderer);

    ktnApplicationWindow window(vk::Extent2D(640, 480));
    window.WindowName = "Box";
    window.Initialize(game);
    window.HideCursor();

    auto graphicsScene = make_shared<ktnGraphicsScene>();
    auto physicsScene = make_shared<ktnPhysicsScene>();
    auto scene = make_shared<bbScene>(graphicsScene, physicsScene, make_shared<bbScene::bbSceneInputProcessor>(window.Window));
    // This is particularly leaking memory.
    // If we create normal variables here they will go out of scope after the if
    // We keep it for the time being, as the vulkan backend needs to have the
    // shader and model classes changed as well.
    if (game->Renderer->Backend() == ktnGraphicsBackend::OPENGL) {
        auto basic_cube = make_shared<ktnModel>("resources/models/cube_basic.gltf");
        basic_cube->Name = "basic_cube";
        basic_cube->SetPosition({0, 0.2F, 0.5F}, WorldSpace);
        basic_cube->SetScale(0.2F, WorldSpace);
        graphicsScene->AddModel(basic_cube);

        auto basic_monkey = make_shared<ktnModel>("resources/models/monkey_basic.gltf");
        basic_monkey->Name = "basic_monkey";
        basic_monkey->SetPosition({1, 5, 1}, WorldSpace);
        basic_monkey->SetRotation(normalize(angleAxis(45.F, vec3(0.5, 0.5, 0.5))), WorldSpace);
        basic_monkey->SetScale(0.2F, WorldSpace);
        graphicsScene->AddModel(basic_monkey);

        auto basic_cube_big = make_shared<ktnModel>("resources/models/cube_basic.gltf");
        basic_cube_big->Name = "basic_cube_big";
        basic_cube_big->SetScale(vec3(3, 3, 3), WorldSpace);
        basic_cube_big->SetPosition({0, -3, 0}, WorldSpace);
        graphicsScene->AddModel(basic_cube_big);

        //
        //
        // experimental physics integration
        {
            basic_cube_big->InitializePhysicalObject(10000, false);
            physicsScene->AddObject(basic_cube_big->PhysicalObject);

            basic_monkey->InitializePhysicalObject(10000, true);
            physicsScene->AddObject(basic_monkey->PhysicalObject);
        }
        //
        //
        //

        auto basic_stairs = make_shared<ktnModel>("resources/models/stairs_basic.gltf");
        basic_stairs->Name = "basic_stairs";
        graphicsScene->AddModel(basic_stairs);
    }

    game->SetActiveScene(scene);

    // render loop
    window.Run();
    // some functions to deallocate all memories taken by the assets
    return 0;
}
