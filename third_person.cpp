#include <ktnEngine/ktnApplication>
#include <ktnEngine/ktnGraphics>

using namespace glm;
using namespace ktn;
using namespace ktn::Application;
using namespace ktn::Core;
using namespace ktn::Graphics;
using namespace ktn::Physics;
using namespace std;

class tpScene : public ktnScene {
public:
    class tpSceneInputProcessor : public IktnInputProcessor {
    public:
        tpSceneInputProcessor(GLFWwindow *eWindow) {
            m_Window = eWindow;
            m_InputMapper.Initialize();
        }
        void ProcessInput() {
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_ESCAPE]) == GLFW_PRESS) { glfwSetWindowShouldClose(m_Window, 1); }
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_W]) == GLFW_PRESS) { KeyPressed.Emit(KTNKEY_W); }
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_A]) == GLFW_PRESS) { KeyPressed.Emit(KTNKEY_A); }
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_S]) == GLFW_PRESS) { KeyPressed.Emit(KTNKEY_S); }
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_D]) == GLFW_PRESS) { KeyPressed.Emit(KTNKEY_D); }
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_W]) == GLFW_RELEASE) { KeyReleased.Emit(KTNKEY_W); }
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_A]) == GLFW_RELEASE) { KeyReleased.Emit(KTNKEY_A); }
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_S]) == GLFW_RELEASE) { KeyReleased.Emit(KTNKEY_S); }
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_D]) == GLFW_RELEASE) { KeyReleased.Emit(KTNKEY_D); }

            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_UP]) == GLFW_PRESS) { KeyPressed.Emit(KTNKEY_UP); }
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_DOWN]) == GLFW_PRESS) { KeyPressed.Emit(KTNKEY_DOWN); }
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_LEFT]) == GLFW_PRESS) { KeyPressed.Emit(KTNKEY_LEFT); }
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_RIGHT]) == GLFW_PRESS) { KeyPressed.Emit(KTNKEY_RIGHT); }
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_SPACE]) == GLFW_PRESS) { KeyPressed.Emit(KTNKEY_SPACE); }
            if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_LEFT_CONTROL]) == GLFW_PRESS) { KeyPressed.Emit(KTNKEY_LEFT_CONTROL); }
        }

        ktnSignal<ktnKey> KeyPressed;
        ktnSignal<ktnKey> KeyReleased;

    private:
        GLFWwindow *m_Window;

        ktnInputMapper m_InputMapper;
    };

    tpScene(ktnGraphicsScenePtr gScene, ktnPhysicsScenePtr pScene, shared_ptr<tpSceneInputProcessor> inputProcessor) : ktnScene(gScene, pScene) {
        InputProcessor = inputProcessor;
        inputProcessor->KeyPressed.Connect(this, &tpScene::HandleEvent_KeyPressed);
        inputProcessor->KeyReleased.Connect(this, &tpScene::HandleEvent_KeyReleased);

        auto camera = make_shared<ktnCamera>();

        camera->EulerAngles.Pitch = 50;
        camera->OffsetPosition({1, 1, 1}, ReferenceSpace::World);
        camera->TurnLeft(450);
        camera->TurnDown(150);

        m_GraphicsScene->SetCurrentCamera(camera);

        srand(static_cast<unsigned int>(std::chrono::steady_clock::now().time_since_epoch().count()));

        for (int i = 0; i < 50; ++i) {
            auto light = make_shared<ktnLight>((string("light") + to_string(i)).c_str());
            light->Intensity = 0.1f * (rand() % 10);
            light->SetPosition(vec3(0.1f * (rand() % 100 - 50), 0.1f * (rand() % 100 - 50), 0.1f * (rand() % 100 - 50)), WorldSpace);
            light->SetColor(0.01f * (rand() % 100), 0.01f * (rand() % 100), 0.01f * (rand() % 100));
            light->Falloff.LinearTerm = 0.5f;
            light->Falloff.QuadraticTerm = 0.5f;
            m_GraphicsScene->AddLight(light);
        }

        m_GraphicsScene->Lights[0]->SetPosition(
            m_GraphicsScene->CurrentCamera()->Position(WorldSpace) + m_GraphicsScene->CurrentCamera()->Front() * 0.1f, WorldSpace);
        m_GraphicsScene->Lights[0]->SetDirection(m_GraphicsScene->CurrentCamera()->Front());
        m_GraphicsScene->Lights[0]->Intensity = 100;

        auto basic_cube = make_shared<ktnModel>("resources/models/cube_basic.gltf");
        basic_cube->Name = "basic_cube";
        basic_cube->SetPosition({0, 0.2F, 0.5F}, WorldSpace);
        basic_cube->SetScale(0.2F, WorldSpace);
        m_GraphicsScene->AddModel(basic_cube);

        auto basic_monkey = make_shared<ktnModel>("resources/models/monkey_basic.gltf");
        basic_monkey->Name = "basic_monkey";
        basic_monkey->SetPosition({1, 5, 1}, WorldSpace);
        basic_monkey->SetRotation(angleAxis(45.F, vec3(0.5, 0.5, 0.5)), WorldSpace);
        basic_monkey->SetScale(0.2F, WorldSpace);
        m_GraphicsScene->AddModel(basic_monkey);

        auto basic_cube_big = make_shared<ktnModel>("resources/models/cube_basic.gltf");
        basic_cube_big->Name = "basic_cube_big";
        basic_cube_big->SetScale(vec3(3, 3, 3), WorldSpace);
        basic_cube_big->SetPosition({0, -3, 0}, WorldSpace);
        m_GraphicsScene->AddModel(basic_cube_big);

        //
        //
        // experimental bullet integration
        {
            basic_cube_big->InitializePhysicalObject(1000, false);
            m_PhysicsScene->AddObject(basic_cube_big->PhysicalObject);

            basic_monkey->InitializePhysicalObject(1000, true);
            m_PhysicsScene->AddObject(basic_monkey->PhysicalObject);
        }
        //
        //
        //

        auto basic_stairs = make_shared<ktnModel>("resources/models/stairs_basic.gltf");
        basic_stairs->Name = "basic_stairs";
        m_GraphicsScene->AddModel(basic_stairs);

        auto model = make_shared<ktnModel>("resources/models/placeholder_robot.gltf", "character");
        CurrentActor = model;

        camera->SetParent(model.get());

        m_GraphicsScene->AddModel(model);
    }

    void HandleEvent_KeyPressed(ktnKey eKey) {
        switch (eKey) {
        case KTNKEY_W: {
            dynamic_cast<ktnModel *>(CurrentActor.get())->StartAction("walk");
            auto cameraFrontFlat = vec3(m_GraphicsScene->CurrentCamera()->Front().x, 0, m_GraphicsScene->CurrentCamera()->Front().z);
            cameraFrontFlat *= 0.01;
            CurrentActor->OffsetPosition(cameraFrontFlat, WorldSpace);
        } break;
        case KTNKEY_S: {
            auto cameraFrontFlat = vec3(m_GraphicsScene->CurrentCamera()->Front().x, 0, m_GraphicsScene->CurrentCamera()->Front().z);
            cameraFrontFlat *= 0.01;
            CurrentActor->OffsetPosition(-cameraFrontFlat, WorldSpace);
        } break;
        case KTNKEY_A: {
            auto cameraRightFlat = vec3(m_GraphicsScene->CurrentCamera()->Right().x, 0, m_GraphicsScene->CurrentCamera()->Right().z);
            cameraRightFlat *= 0.01;
            CurrentActor->OffsetPosition(-cameraRightFlat, WorldSpace);
        } break;
        case KTNKEY_D: {
            auto cameraRightFlat = vec3(m_GraphicsScene->CurrentCamera()->Right().x, 0, m_GraphicsScene->CurrentCamera()->Right().z);
            cameraRightFlat *= 0.01;
            CurrentActor->OffsetPosition(cameraRightFlat, WorldSpace);
        } break;
        }
    }

    void HandleEvent_KeyReleased(ktnKey eKey) {
        switch (eKey) {
            /*case KTNKEY_W: {
                dynamic_cast<ktnModel *>(CurrentActor.get())->StopAction("walk");
            } break;
             case KTNKEY_S: {
                 dynamic_cast<ktnModel *>(CurrentActor.get())->StopAction("walk");
             } break;
             case KTNKEY_A: {
                 dynamic_cast<ktnModel *>(CurrentActor.get())->StopAction("walk");
             } break;
             case KTNKEY_D: {
                 dynamic_cast<ktnModel *>(CurrentActor.get())->StopAction("walk");
             } break;*/
        }
    }

    void HandleEvent_MouseMoved(double xpos, double ypos) final {
        if (m_FirstMouse) {
            m_LastX = xpos;
            m_LastY = ypos;
            m_FirstMouse = false;
        }
        m_GraphicsScene->CurrentCamera()->TurnRight(float(xpos - m_LastX));
        m_GraphicsScene->CurrentCamera()->TurnDown(float(ypos - m_LastY));
        m_GraphicsScene->CurrentCamera()->UpdateFront();
        m_GraphicsScene->CurrentCamera()->UpdateViewMatrix();
        m_LastX = xpos;
        m_LastY = ypos;
    }

    void ProcessInput() final {
        auto currentFrameTime = static_cast<float>(glfwGetTime());
        m_DeltaTime = currentFrameTime - m_LastFrameTime;
        m_LastFrameTime = currentFrameTime;

        InputProcessor->ProcessInput();

        m_GraphicsScene->CurrentCamera()->UpdateViewMatrix();

        if (m_LeftMouseIsClicking) {
            m_GraphicsScene->Lights[0]->SetPosition(
                m_GraphicsScene->CurrentCamera()->Position(WorldSpace) + m_GraphicsScene->CurrentCamera()->Front() * 0.1f, WorldSpace);
            m_GraphicsScene->Lights[0]->SetDirection(m_GraphicsScene->CurrentCamera()->Front());
        }

        for (size_t i = 1; i < m_GraphicsScene->Lights.size(); ++i) {
            auto light = m_GraphicsScene->Lights[i].get();
            light->Intensity = std::clamp(light->Intensity + 0.01f * (rand() % 10 - 5), 0.5f, 5.0f);
            light->OffsetPosition(vec3(0.001f * (rand() % 100 - 50), 0.001f * (rand() % 100 - 50), 0.001f * (rand() % 100 - 50)), WorldSpace);
            if (abs(light->Position(WorldSpace).x) > 4) { //
                light->SetPosition(light->Position(WorldSpace) / 4.0F, WorldSpace);
            }
            if (abs(light->Position(WorldSpace).z) > 4) { light->SetPosition(light->Position(WorldSpace) / 4.0F, WorldSpace); }

            if (light->Position(WorldSpace).y > 2) { light->SetPosition(light->Position(WorldSpace) / 2.0F, WorldSpace); }
            if (light->Position(WorldSpace).y < 0) { light->OffsetPosition({0, 0.5, 0}, WorldSpace); }
        }
    }

    shared_ptr<ktn3DElement> CurrentActor;
};

int main() {
    auto app = ktnApplication::CreateApplicationWithDefaultRenderer<ktnRenderer_OpenGL_ImGui>();

    auto window = ktnApplicationWindow({1024, 768});
    window.Initialize(app);
    window.HideCursor();

    auto scene = make_shared<tpScene>( //
        make_shared<ktnGraphicsScene>(),
        make_shared<ktnPhysicsScene>(),
        make_shared<tpScene::tpSceneInputProcessor>(window.Window));

    app->SetActiveScene(scene);
    window.Run();
}
